# What's here
In questo repository verranno raccolti una serie di tool a supporto dello *smart scan* sul goniometro utilizzato durante i test beam del gruppo Insulab.  

- La cartella [PITONE](./PITONE/) contiene gli script necessari a generare il file di punti che verrà letto dal software di acquisizione, e che rappresenta la mappa di punti che verranno battuti durante la scansione. Il file di output contiene due colonne di float, la prima per il **crad** e la seconda per il **rot**.
- La cartella [ANALISI](./ANALISI/) conterrà alcuni script per una rapida analisi della scansione
- [functions.py](./PITONE/functions.py) contiene tutte le funzioni che possono essere chiamate dalla [CLI (Command Line Interface)](./PITONE/CLI.py) o dalla [GUI (Graphical User Interface)](./PITONE/GUI.py) quando sarà pronta.


# Modalità di generazione punti
Sono ipotizzate due modalità di scansione
## Diagonal
Si genera un vettore di $n$ punti per il crad e altrettanti per il rot, specificando **inizio**, **fine** e alternativamente **numero di punti** o **step di scansione**. Verrà quindi eseguita la mappa
```
crad[1] rot[1]
crad[2] rot[2]
crad[3] rot[3]
crad[4] rot[4]
...
```

## Meshgrid
Si genera un vettore $n$ punti per il crad e un vettore di $m$ punti per il rot, specificando **inizio**, **fine** e alternativamente **numero di punti** o **step di scansione**. Verrà quindi eseguita la mappa
```
crad[1] rot[1]
crad[1] rot[2]
...
crad[1] rot[m]
crad[2] rot[1]
crad[2] rot[2]
...
crad[2] rot[m]
...
...
crad[n] rot[m]
```


# TODO
- Debuggare la [CLI](./PITONE/CLI.py) per verificare che ogni sua parte funzioni
- Implementare la [GUI](./PITONE/GUI.py)  
