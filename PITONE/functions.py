import sys, os, re, glob
import numpy as np


def diagonalScan(startCrad: float, endCrad: float, startRot: float, endRot: float, nPts) -> np.ndarray:
    """diagonalScan produces the map of points for a diagonal scan. 
    
    At the moment only the number of steps can be provided.
    The end point is included.
    
    Args:
        startCrad (_type_): starting position for the cradle
        endCrad (_type_): end position for the cradle
        startRot (_type_): start position for the rotational
        endRot (_type_): end position for the rotational
        nPts (_type_): number of steps for the scan
    """
    
    # Patch for accepting a vector
    try:
        nPts = nPts[0]
    except:
        pass
    
    
    # Creo i due vettori specificando il numero di punti
    vectCrad = np.linspace(startCrad, endCrad, nPts)        
    vectRot = np.linspace(startRot, endRot, nPts)  

    # Impacchetto in una matrice Nx2
    return(np.concatenate((vectCrad[:, np.newaxis], vectRot[:, np.newaxis]), axis = 1))
 


def meshgridScan(startCrad: float, endCrad: float, startRot: float, endRot: float, nPts = None, step = None) -> np.ndarray:
    """meshgridScan produces the map of points for a mesh grid scan.
    
    If nPts is provided as a single integer, then a meshgrid of nPts x nPts equally spaced
    points will be generates
    
    If nPts is provided as a list, then its first element is the number of points for the cradle
    and the second for the rotational. Other values will be ignored
    
    if nPts IS NOT provided, then the step must be specified, either as a single value or a list,
    where the first two elements will be used respectively for the cradle and the rotational.
    In the step case, the end point IS NOT included, due to the np.arange underlying method.

    Args:
        startCrad (_type_): starting position for the cradle
        endCrad (_type_): end position for the cradle
        startRot (_type_): start position for the rotational
        endRot (_type_): end position for the rotational
        nPts (_type_, optional): int or list for the number of points for the meshgrid. Defaults to None.
        step (_type_, optional): float or list for the step of the meshgrid only IF nPts IS NOT provided. Defaults to None.
    """
    
    if nPts is not None:
        try:
            nPtsCrad = nPts[0]
            nPtsRot = nPts[1]
            
        # TypeError: is not a list, IndexError: less than two elements in the list
        except IndexError:
            nPtsCrad = nPts[0]
            nPtsRot = nPts[0]
        except TypeError:
            nPtsCrad = nPts
            nPtsRot = nPts
            
        # Creo i due vettori specificando il numero di punti
        vectCrad = np.linspace(startCrad, endCrad, nPtsCrad)        
        vectRot = np.linspace(startRot, endRot, nPtsRot)  


    else:
        try:
            stepCrad = step[0]
            stepRot = step[1]
            
        # TypeError: is not a list, IndexError: less than two elements in the list
        except IndexError:
            stepCrad = step[0]
            stepRot = step[0]
        except TypeError:
            stepCrad = step
            stepRot = step
            
        # Creo i due vettori specificando lo step
        vectCrad = np.arange(startCrad, endCrad, stepCrad)        
        vectRot = np.arange(startRot, endRot, stepRot)  


    # Creo la meshgrid e appiattisco per ottenere il vettore di x e y
    mx, my = np.meshgrid(vectCrad, vectRot, indexing='ij')
    mx_flat = mx.flatten(order = "C")
    my_flat = my.flatten(order = "C")
    
    
    # Impacchetto in una matrice Nx2
    return(np.concatenate((mx_flat[:, np.newaxis], my_flat[:, np.newaxis]), axis = 1))




def saveScan(fileName, data, overwrite = False):
    """saveScan saves a map of points in a file

    Args:
        fileName (_type_): path of the file
        data (_type_): matrix with two column that will be saved
        overwrite (bool, optional): Decide if force overwriting existing file. Defaults to False.
    """
    
    
    # Controllo di consistenza che i dati siano come mi aspetto
    assert(len(data.shape) == 2)
    assert(data.shape[1] == 2)
    
    
    # Salvataggio con protezione da sovrascrittura
    if os.path.exists(fileName) and not overwrite:
        raise Exception("Cannot overwiite existing file unless 'overwrite' argument set to True")
    else:
        np.savetxt(fileName, data, delimiter = "\t", newline = "\n")