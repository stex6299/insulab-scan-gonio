#!/usr/bin/env python3

import argparse
from functions import *

parser = argparse.ArgumentParser(
                    prog='Insulab Goniometer Scan generator',
                    description='CLI for producing the map for the scan of the goniometer',
                    epilog='Text at the bottom of help')

# Starting and ending position
parser.add_argument("startCrad", 
                    type = float,
                    help = "Starting position for the cradle")
parser.add_argument("endCrad", 
                    type = float,
                    help = "Ending position for the cradle")
parser.add_argument("startRot", 
                    type = float,
                    help = "Starting position for the rotational")
parser.add_argument("endRot", 
                    type = float,
                    help = "Ending position for the rotational")

# Force overwrite
parser.add_argument("-f", 
                    "--force-overwrite", 
                    "--fo", 
                    dest = 'forceOverwrite',
                    action = "store_true",
                    help = "Decide wether to force the overwriting of existing file")

# Output file
parser.add_argument("-o", 
                    "--output-file",
                    "--of",
                    dest = "outputFile",
                    required=True,
                    help = "Path for the output file")


# Step or number of points
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument("-s",
                   "--step",
                   dest = "step",
                   nargs="+",
                   type = float,
                #    metavar = "Step [stepCrad, stepRot]",
                   help = "Step (can be two items for the meshgrid mode)")
group.add_argument("-n",
                   "--number-points",
                   "--npts",
                   dest = "nPts",
                   nargs="+",
                   type = int,
                #    metavar = "nPts [nPtsCrad, nPtsRot]",
                   help = "number of points (can be two items for the meshgrid mode)")


# group1 = parser.add_mutually_exclusive_group(required=True)
# group1.add_argument("--diagonal-mode",
#                     "--dm",
#                     "-d",
#                     dest = "diagonalMode",
#                     action="store_true")
# group1.add_argument("--meshgrid-mode",
#                     "--mm",
#                     "--mgm",
#                     "-m",
#                     dest = "diagonalMode",
#                     action="store_true",)


parser.add_argument("-m",
                    "--mode",
                    dest = "mode",
                    choices=["diagonal", "meshgrid"], 
                    default="diagonal",
                    help = "Choose between diagonal or meshgrid mode. Default = diagonal.")

args = parser.parse_args()




# Call to the functions

# Diagonal mode
if args.mode == "diagonal":
    
    if args.step is not None:
        raise Exception("At the moment it is not possible to perform diagonal scan providing the range")
        

    data = diagonalScan(startCrad = args.startCrad, endCrad = args.endCrad, 
                        startRot = args.startRot, endRot = args.endRot, 
                        nPts = args.nPts)

# Meshgrid mode
else:
    data = meshgridScan(startCrad = args.startCrad, endCrad = args.endCrad, 
                        startRot = args.startRot, endRot = args.endRot, 
                        nPts = args.nPts, step = args.step)


saveScan(args.outputFile, data, overwrite = args.forceOverwrite)
print(f"I have written {args.outputFile}")