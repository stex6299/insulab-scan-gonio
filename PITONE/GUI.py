#!/usr/bin/env python3 

from mainwindow import *
from PyQt5.QtCore import pyqtSlot
# from PyQt5.QtGui import *
# from PyQt5.QtWidgets import *
# from PyQt5.Qt import Qt


import sys, os, re, glob
from functions import *


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        # self.btnSource.clicked.connect(self.printMe)
        
    
        
    # @pyqtSlot()
    # def on_bottone_clicked(self):
    #     qm = QtWidgets.QMessageBox
    #     ret = qm.warning(self,'DANGER OVERWRITING', "Are you sure tooverwrite existing file?", qm.Yes | qm.No)
        
        
    # Esempio, genera il segnale di salva
    # def keyPressEvent(self, e):
    #     # if e.key() == Qt.Key_F5:
    #     if e.key() == Qt.Key_E:
    #         # self.close()
    #         self.btn_diagonal_save.clicked.emit()


        
    
        
    @pyqtSlot()
    def on_btn_diagonal_save_clicked(self):
        
        # TODO: Validate the characters
        
        # Create data vector
        data = diagonalScan(startCrad = float(self.txt_diagonal_crad_start.text()),
                            endCrad = float(self.txt_diagonal_crad_end.text()), 
                            startRot = float(self.txt_diagonal_rot_start.text()), 
                            endRot = float(self.txt_diagonal_rot_end.text()), 
                            nPts = int(self.txt_diagonal_npoints.text())
                            )
        
        # Name of the output file
        fileName = self.txt_diagonal_path.text()
        
        
        if os.path.exists(fileName):
            
            # MessageBox to protect overwriting
            qm = QtWidgets.QMessageBox
            ret = qm.warning(self,'DANGER OVERWRITING', "Are you sure to overwrite existing file?", qm.Yes | qm.No, defaultButton=qm.No)
            
            if ret == qm.Yes: 
                saveScan(fileName, data, overwrite = True)
                print(f"I have overwritten {fileName}")
        else:
            saveScan(fileName, data, overwrite = False)
            print(f"I have written {fileName}")
        
        print(data)



if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    app.setApplicationName("My amazing GUI")

    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())